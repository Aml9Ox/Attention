"""
This contains data paramenters
"""
class Config:
  def __init__(self):
    self.de_en_data_sources = [
                          {
                              "url": "http://www.statmt.org/europarl/v7/de-en.tgz",
                              "input": "europarl-v7.de-en.en",
                              "target": "europarl-v7.de-en.de",
                           }]
    self.de_en_evaluation_data = [
        
                          {
                              "url": "http://data.statmt.org/wmt17/translation-task/dev.tgz",
                              "input": "newstest2013.en",
                              "target": "newstest2013.de",
                          }]
    self.fr_en_data_sources = [
                          {
                              "url": "http://www.statmt.org/europarl/v7/fr-en.tgz",
                              "input": "europarl-v7.fr-en.en",
                              "target": "europarl-v7.fr-en.fr",
                          }]
    self.fr_en_evaluation_data = []
    
